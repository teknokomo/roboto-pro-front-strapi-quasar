module.exports = {
  // https://eslint.org/docs/user-guide/configuring#configuration-cascading-and-hierarchy
  // This option interrupts the configuration hierarchy at this file
  // Remove this if you have an higher level ESLint config file (it usually happens into a monorepos)
  root: true,

  parserOptions: {
    parser: '@babel/eslint-parser',
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module' // Allows for the use of imports
  },

  env: {
    browser: true
  },

  // Rules order is important, please avoid shuffling them
  extends: [
    // Base ESLint recommended rules
    // 'eslint:recommended',

    // Uncomment any of the lines below to choose desired strictness,
    // but leave only one uncommented!
    // See https://eslint.vuejs.org/rules/#available-rules
    'plugin:vue/vue3-essential', // Priority A: Essential (Error Prevention)
    // 'plugin:vue/vue3-strongly-recommended', // Priority B: Strongly Recommended (Improving Readability)
    // 'plugin:vue/vue3-recommended', // Priority C: Recommended (Minimizing Arbitrary Choices and Cognitive Overhead)

    // https://github.com/prettier/eslint-config-prettier#installation
    // usage with Prettier, provided by 'eslint-config-prettier'.
    'prettier'
  ],

  plugins: [
    // https://eslint.vuejs.org/user-guide/#why-doesn-t-it-work-on-vue-file
    // required to lint *.vue files
    'vue'

    // https://github.com/typescript-eslint/typescript-eslint/issues/389#issuecomment-509292674
    // Prettier has not been included as plugin to avoid performance impact
    // add it as an extension for your IDE
  ],

  globals: {
    ga: 'readonly', // Google Analytics
    cordova: 'readonly',
    __statics: 'readonly',
    __QUASAR_SSR__: 'readonly',
    __QUASAR_SSR_SERVER__: 'readonly',
    __QUASAR_SSR_CLIENT__: 'readonly',
    __QUASAR_SSR_PWA__: 'readonly',
    process: 'readonly',
    Capacitor: 'readonly',
    chrome: 'readonly'
  },

  // add your custom rules here
  rules: {
    // semi: [2, 'always'],
    camelcase: 'off',
    'prefer-const': 'warn',
    'no-empty-pattern': 'warn',
    'no-prototype-builtins': 'warn',
    'no-unused-expressions': 'warn',
    'no-unused-vars': 'off',
    'no-useless-escape': 'warn',
    'no-void': 'warn',
    'handle-callback-err': 'off',
    'node/handle-callback-err': 'off',
    'vue/no-deprecated-v-on-native-modifier': 'warn',
    'vue/no-mutating-props': 'off',
    'vue/no-unused-components': 'warn',
    'vue/multi-word-component-names': 'off',
    'vue/require-valid-default-prop': 'warn',
    'vue/valid-v-for': 'warn',
    'vue/no-setup-props-destructure': 'warn',
    'vue/require-v-for-key': 'warn',
    'vue/no-use-v-if-with-v-for': 'warn',
    'vue/no-computed-properties-in-data': 'warn',
    'vue/no-parsing-error': [
      'warn',
      {
        // "abrupt-closing-of-empty-comment": true,
        // "absence-of-digits-in-numeric-character-reference": true,
        // "cdata-in-html-content": true,
        // "character-reference-outside-unicode-range": true,
        // "control-character-in-input-stream": true,
        // "control-character-reference": true,
        // "eof-before-tag-name": true,
        // "eof-in-cdata": true,
        // "eof-in-comment": true,
        // "eof-in-tag": true,
        // "incorrectly-closed-comment": true,
        // "incorrectly-opened-comment": true,
        'invalid-first-character-of-tag-name': false
        // "missing-attribute-value": true,
        // "missing-end-tag-name": true,
        // "missing-semicolon-after-character-reference": true,
        // "missing-whitespace-between-attributes": true,
        // "nested-comment": true,
        // "noncharacter-character-reference": true,
        // "noncharacter-in-input-stream": true,
        // "null-character-reference": true,
        // "surrogate-character-reference": true,
        // "surrogate-in-input-stream": true,
        // "unexpected-character-in-attribute-name": true,
        // "unexpected-character-in-unquoted-attribute-value": true,
        // "unexpected-equals-sign-before-attribute-name": true,
        // "unexpected-null-character": true,
        // "unexpected-question-mark-instead-of-tag-name": true,
        // "unexpected-solidus-in-tag": true,
        // "unknown-named-character-reference": true,
        // "end-tag-with-attributes": true,
        // "duplicate-attribute": true,
        // "end-tag-with-trailing-solidus": true,
        // "non-void-html-element-start-tag-with-trailing-solidus": false,
        // "x-invalid-end-tag": true,
        // "x-invalid-namespace": true
      }
    ],
    // '': 'warn',
    // '': 'warn',

    'no-console': 'off',

    // 'prettier/prettier': ['error', {}, {}],

    'prefer-promise-reject-errors': 'off',

    // allow debugger during development only
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
};
