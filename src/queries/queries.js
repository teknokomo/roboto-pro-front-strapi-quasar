import gql from 'graphql-tag';
// полный список страниц
export const pages = gql`
  query {
    pages {
      data {
        id
        attributes {
          title
          text
          tabName
          background {
            horizontal {
              data {
                attributes {
                  url
                  width
                  height
                  formats
                }
              }
            }
            vertical {
              data {
                attributes {
                  url
                  width
                  height
                  formats
                }
              }
            }
          }
          seo {
            title
            description
            meta {
              name
              content
            }
          }
        }
      }
    }
  }
`;
// получение статических данных (лого итд)
export const other = gql`
  query {
    other {
      data {
        attributes {
          logo {
            data {
              attributes {
                url
                width
                height
                size
                formats
              }
            }
          }
          acceptCookies
        }
      }
    }
  }
`;
// получение списка аудиофайлов
export const musics = gql`
  query {
    musics {
      data {
        attributes {
          audiofile {
            data {
              attributes {
                url
              }
            }
          }
        }
      }
    }
  }
`;
